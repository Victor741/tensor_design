import React, { useState } from "react";
import "./App.scss";
import {
  TensorButton,
  TensorCheckBoxGroup,
  TensorRadioGroup,
  TensorSwitch,
  TensorTextarea,
  TensorSelect,
  TensorInput,
  TensorModalHasActionButton,
  TensorModalNormal,
  TensorJson,
  TensorTable,
  TensorEllipsis,
  // TensorPopover,
} from "./designLibrary/index";
// } from "tensor_design";


const App = () => {
  const [val, setval] = useState(undefined as string | string[] | undefined);
  const [visible, setvisible] = useState(false);

  const [cval, setcval] = useState(undefined as string[] | undefined);

  setTimeout(()=>{
    setcval(['1234'])
  },3000)
  return (
    <div className="App">
      <TensorButton
        size="middle"
        btnType="primary"
        // ico={require("../../public/logo192.png")}
        // href={"12323434"}
        loading={true}
        onClick={() => alert(1)}
      >
        按钮
      </TensorButton>
      <TensorCheckBoxGroup
        name="test"
        tensorCheckBoxList={[
          { label: "123", value: "123" },
          { label: "1234", value: "1234" },
        ]}
        defaultValue={['123']}
        value={cval}
        onChange={(val)=>{setcval(val)}}
      />
      <TensorRadioGroup
        name="test2"
        tensorRadioList={[
          { label: "123", value: "123" },
          { label: "1234", value: "1234" },
        ]}
      />
      <TensorSwitch
        name="test3"
        tensorSwitchList={[
          { label: "123", value: "123" },
          { label: "1234", value: "1234" },
        ]}
      />
      <TensorTextarea value={"123213"} onChange={() => {}} />
      <TensorSelect
        name="test3"
        tensorSelectList={[
          { label: "aa", value: "123" },
          { label: "bb", value: "1234" },
        ]}
        // onChange={(e) => {
        //   console.log(e.target.value);
        //   Array.from(e.target).forEach((item: any) => {
        //     console.log(item.value);
        //   });
        // }}
        onValChange={(val) => {
          console.log(val);
          setval(val);
        }}
        // multiple={true}
        optionComp={(p) => <div>{p.value}</div>}
        value={val}
        // disabled={true}
        // defaultValue={["1234"]}
        placeholder="placeholder"
        boxsize="small"
        hideWhenClickOut={false}
      />
      <TensorInput
        value={"123213"}
        onChange={(e) => {
          console.log(e);
        }}
        label="哈哈哈哈哈哈"
      />
      <TensorModalHasActionButton
        buttonProps={{ children: "按钮" }}
        contentComp={
          <TensorModalNormal
            cancleBtnProps={{ children: "取消" }}
            sureBtnProps={{ children: "确定" }}
            title="标题"
            content={
              <>
                何时使用：需要用户处理事务，又不希望跳转页面以致打断工作流程时，可以使用
                Modal 在当前页面正中打开一个浮层，承载相应的操作。
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
              </>
            }
            onSure={() => {
              alert(1);
            }}
          />
        }
      />
      <TensorJson json={{ aa: 1, bb: "23423324" }} type="pre" />
      <TensorTable
        data={[
          {
            namespaces: "s1",
            snv_name: "svc1",
          },
          {
            namespaces: "s1",
            snv_name: "svc2",
          },
          {
            namespaces: "s1",
            snv_name: "svc3",
          },
        ]}
        columns={[
          {
            name: "namespaces",
            selector: "namespaces",
          },
          {
            name: "snv_name",
            selector: "snv_name",
          },
        ]}
      />
      <TensorEllipsis
        maxRow={1}
        ellipsisStr={"..."}
        onEllipsis={(ellipsis, ellipsisText, fullText) => {
          console.log(ellipsis, ellipsisText, fullText);
        }}
      >
        对于科技人员来说，这本书真的可谓是学习架构设计的福星。设计架构在很多人眼中是一个高深的技术，对上需要掌握业务模式、功能需求，对下需要把控开发模式、数据库、储存技术，还需要考虑安全控制、监控模式，同时还要有颗与时俱进、不断学习新技术的心。只有把这些技术融会贯通、了然于胸，才能设计出业务部门满意、技术人员不吐槽的架构。对于大多数来说，架构设计的确高深莫测、错综复杂，想学习架构设计困难重重。而这本书可以改变了我之前的想法，架构设计也是有章可循，也是可以通过学习掌握，即使没有大神般的技术能力、丰富的技术知识积累，也可以顺着作者的逻辑，看出架构设计的门道来。不敢说看完此书就能成为一名系统架构师，但是照着学习一定悟到架构设计门道来
      </TensorEllipsis>
      {/* <TensorPopover
        content={<div>TensorPopover content</div>}
        trigger="click"
        visible={visible}
        onVisibleChange={(visible) => {
          console.log(visible)
          setvisible(visible);
        }}
      >
        <div>TensorPopover</div>
      </TensorPopover> */}
    </div>
  );
};

export default App;
