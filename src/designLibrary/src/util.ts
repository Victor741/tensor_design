export function documentOffset(element: any) {
  const pos = { left: 0, top: 0 };

  let parents = element.offsetParent;

  pos.left += element.offsetLeft;
  pos.top += element.offsetTop;

  while (parents && !/html|body/i.test(parents.tagName)) {
    pos.left += parents.offsetLeft;
    pos.top += parents.offsetTop;

    parents = parents.offsetParent;
  }
  return pos;
}

export function copyText(text: string) {
  const textarea = document.createElement("textarea");
  textarea.style.position = "absolute";
  textarea.style.left = "-1000px";
  textarea.style.opacity = "0";
  const currentFocus = document.activeElement as any;
  const toolBoxwrap = document.body;
  toolBoxwrap.appendChild(textarea);
  textarea.defaultValue = text;
  textarea.focus();
  if (textarea.setSelectionRange) {
    textarea.setSelectionRange(0, textarea.value.length);
  } else {
    textarea.select();
  }
  let flag: boolean;
  try {
    flag = document.execCommand("copy");
  } catch (eo) {
    flag = false;
  }
  toolBoxwrap.removeChild(textarea);
  currentFocus && currentFocus.focus();
  return flag;
}
