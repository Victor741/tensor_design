import React, { useEffect, useRef, useState } from 'react';
import { useMemo } from 'react';
import './radio.scss';

interface TensorRadioProps
  extends React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > {
  label?: any;
}

const TensorRadioClassName = 'tensorRadio';

export const TensorRadio = (props: TensorRadioProps) => {
  const isDisabled = useMemo(() => {
    if (props.hasOwnProperty('disabled')) {
      if (props.disabled === true || undefined) {
        return true;
      }
      return false;
    }
    return false;
  }, [props]);

  const isChecked = useMemo(() => {
    if (props.hasOwnProperty('checked')) {
      if (props.checked === true || undefined) {
        return true;
      }
      return false;
    }
    return false;
  }, [props]);

  const className = useMemo(() => {
    return `${TensorRadioClassName} ${isChecked ? 'checked' : ''} ${
      isDisabled ? 'disable' : ''
    } ${props.className || ''}`;
  }, [props.className, isDisabled, isChecked]);

  const realProps = useMemo(() => {
    const tempProps = {
      ...props,
      className,
    };
    delete tempProps.type;
    delete tempProps.label;
    return tempProps;
  }, [props, className]);

  return (
    <label className={className}>
      <span className="radio">
        <input {...realProps} type="radio" />
        <span />
      </span>
      <span className="label">{props.label}</span>
    </label>
  );
};

export interface TensorRadioGroupProps {
  name: string;
  onChange?: (val: string) => any;
  defaultValue?: string;
  tensorRadioList: {
    label: string;
    value: string;
    disabled?: boolean;
  }[];
  className?: string;
}

const TensorRadioGroupClassName = 'tensorRadioGroup';
export const TensorRadioGroup = (props: TensorRadioGroupProps) => {
  const { defaultValue = '', onChange, name, tensorRadioList } = props;
  const [checked, setChecked] = useState(defaultValue);

  const onChangeFun = useRef(null as any);
  useEffect(() => {
    onChangeFun.current = onChange;
  }, [onChange]);

  const tensorRadios = useMemo(() => {
    return tensorRadioList.map((item) => {
      const { label, value, disabled } = item;
      return (
        <TensorRadio
          key={value}
          label={label}
          onChange={(e) => {
            const val = e.target.value;
            setChecked(val);
            onChangeFun.current && onChangeFun.current(val);
          }}
          checked={checked === value}
          name={name}
          value={value}
          disabled={disabled}
        />
      );
    });
  }, [tensorRadioList, name, checked]);

  const className = useMemo(() => {
    return `${TensorRadioGroupClassName} ${props.className || ''}`;
  }, [props.className]);

  return <div className={className}>{tensorRadios}</div>;
};

export default TensorRadioGroup;
