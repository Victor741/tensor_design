import React, { useMemo } from "react";
import "./json.scss";
import TensorTextarea from "./textarea";

interface TensorJsonProps {
  className?: string;
  type?: "textarea" | "pre";
  json?: any;
}

const TensorJsonClassName = "tensorJson";

export const TensorJson = (props: TensorJsonProps) => {
  const { json, type = "pre" } = props;

  const jsonText = useMemo(() => {
    if (type === "textarea") {
      return JSON.stringify(json, null, "\t");
    }
  }, [json, type]);

  const jsonDisplay = useMemo(() => {
    if (type === "textarea") {
      return undefined;
    }
    let str = JSON.stringify(json, null, 2);
    str = str
      .replace(/&/g, "&amp;")
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;");
    return str.replace(
      /("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+-]?\d+)?)/g,
      (match) => {
        var cls = "number";
        if (/^"/.test(match)) {
          if (/:$/.test(match)) {
            cls = "key";
          } else {
            cls = "string";
          }
        } else if (/true|false/.test(match)) {
          cls = "boolean";
        } else if (/null/.test(match)) {
          cls = "null";
        }
        return '<span class="' + cls + '">' + match + "</span>";
      }
    );
  }, [json, type]);

  const className = useMemo(() => {
    return `${TensorJsonClassName} ${props.className || ""}`;
  }, [props.className]);

  return type === "pre" ? (
    <pre
      className={className}
      dangerouslySetInnerHTML={{ __html: jsonDisplay! }}
    />
  ) : (
    <TensorTextarea className={className} readOnly={true} value={jsonText} />
  );
};

export default TensorJson;
