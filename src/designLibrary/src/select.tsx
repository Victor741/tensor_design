import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import "./select.scss";
import { isEqual } from "lodash";

const TensorSelectClassName = "tensorSelect";

interface TensorOptionProps
  extends React.DetailedHTMLProps<
    React.OptionHTMLAttributes<HTMLOptionElement>,
    HTMLOptionElement
  > {}

interface TensorSelectProps
  extends React.DetailedHTMLProps<
    React.SelectHTMLAttributes<HTMLSelectElement>,
    HTMLSelectElement
  > {
  tensorSelectList?: TensorOptionProps[];
  optionComp?: (props: TensorOptionProps) => JSX.Element;
  onValChange?: (val: string | string[]) => void;
  boxsize?: "middle" | "small";
  optionShift?: number;
  onClick?: (e: any) => void;
  hideWhenClickOut?: boolean;
  value?: string | string[];
}

export const TensorSelect = (props: TensorSelectProps) => {
  const {
    className: pclassName,
    tensorSelectList,
    onChange,
    onValChange,
    optionComp: OptionComp,
    multiple,
    defaultValue,
    value,
    placeholder,
    boxsize = "middle",
    optionShift = 10,
    onClick,
    hideWhenClickOut = true,
  } = props;
  const isDisabled = useMemo(() => {
    if (props.hasOwnProperty("disabled")) {
      if (props.disabled === true || undefined) {
        return true;
      }
      return false;
    }
    return false;
  }, [props]);

  const selectEl = useRef(null as HTMLSelectElement | null);
  const optionEls = useRef([] as HTMLOptionElement[]);
  const [selectVal, setSelectVal] = useState(
    defaultValue as string | string[] | undefined
  );

  const [showOption, setShowOption] = useState(false);

  const onSelectChange = useCallback(
    (e: any, aimOption?: HTMLOptionElement) => {
      const thisop =
        aimOption ||
        optionEls.current.find(
          (itemop) => itemop && itemop.value === e.target.value
        )!;

      if (!multiple) {
        optionEls.current.forEach((itemo) => {
          if (itemo && itemo !== thisop) {
            itemo.removeAttribute("selected");
          }
        });
      }

      const isselected = thisop.getAttribute("selected");
      if (isselected) {
        thisop.removeAttribute("selected");
      } else {
        thisop.setAttribute("selected", "selected");
      }

      onChange && onChange(e);

      const tempval: Set<string> = new Set();
      optionEls.current.forEach((itemo) => {
        const isselected = itemo && itemo.getAttribute("selected");
        if (isselected) {
          tempval.add(itemo.value);
        }
      });
      const vals = Array.from(tempval);

      let res = undefined;
      if (multiple) {
        res = vals;
      } else {
        res = vals[0] || "";
        setShowOption(false);
      }

      setSelectVal(res);
      onValChange && onValChange(res);
    },
    [onChange, onValChange, multiple]
  );

  const options = useMemo(() => {
    optionEls.current = [];
    return tensorSelectList?.map((opprops: any, index) => {
      return (
        <option
          {...opprops}
          key={`op_${opprops.value!}_${index}`}
          ref={(el) => optionEls.current.push(el!)}
        />
      );
    });
  }, [tensorSelectList]);

  const tensorOptions = useMemo(() => {
    let tempSelectVal: string[] = selectVal as any;
    if (!Array.isArray(selectVal)) {
      tempSelectVal = [selectVal as string];
    }

    return tensorSelectList?.map((opprops, index) => {
      const selectcb = (e: any) => {
        e.target = optionEls.current;
        e.target.value = opprops.value;

        const thisop = optionEls.current[index];

        onSelectChange(e, thisop);
      };

      const isSelected = tempSelectVal.includes(
        (opprops.value as unknown) as string
      );
      return (
        <div
          className={`optionitem ${isSelected ? "selected" : ""}`}
          key={`tenop_${opprops.value!}_${index}`}
          onClick={selectcb}
        >
          {OptionComp ? <OptionComp {...opprops} /> : <>{opprops.label}</>}
        </div>
      );
    });
  }, [tensorSelectList, onSelectChange, OptionComp, selectVal]);

  const selectValTemp = useRef(selectVal);
  useEffect(() => {
    selectValTemp.current = selectVal;

    let temp: string[] = selectVal as any;
    if (!Array.isArray(selectVal)) {
      temp = [selectVal as string];
    }

    optionEls.current.forEach((itemo) => {
      if (!itemo) {
        return;
      }
      if (temp.includes(itemo.value)) {
        itemo.setAttribute("selected", "selected");
      } else {
        itemo.removeAttribute("selected");
      }
    });
  }, [selectVal]);

  useEffect(() => {
    if (props.hasOwnProperty("value")) {
      if (isEqual(value, selectValTemp.current)) {
        return;
      }
      setSelectVal(value as any);
    }
  }, [props, value]);

  const isPlaceHolder = useMemo(() => {
    return !selectVal || selectVal.length === 0;
  }, [selectVal]);

  const valueDisplay = useMemo(() => {
    if (isPlaceHolder) {
      return placeholder || `...`;
    }
    if (Array.isArray(selectVal)) {
      return tensorSelectList?.reduce((a, b) => {
        if (selectVal.includes(b.value as string)) {
          return `${a ? `${a},` : ""}${b.label}`;
        }
        return a;
      }, "");
    }
    return tensorSelectList?.find((item) => item.value === selectVal)?.label;
  }, [selectVal, placeholder, isPlaceHolder, tensorSelectList]);

  const className = useMemo(() => {
    return `${TensorSelectClassName} ${boxsize} ${
      showOption ? "showoption" : ""
    }  ${isPlaceHolder ? "placeholder" : ""} ${isDisabled ? "disable" : ""} ${
      pclassName || ""
    }`;
  }, [pclassName, isDisabled, isPlaceHolder, boxsize, showOption]);

  const realProps = useMemo(() => {
    const tempProps = {
      ...props,
      className: undefined,
    };

    tempProps.multiple = true;
    delete tempProps.tensorSelectList;
    delete tempProps.optionComp;
    delete tempProps.onChange;
    delete tempProps.onValChange;
    delete tempProps.value;
    delete tempProps.defaultValue;
    delete tempProps.size;
    delete tempProps.optionShift;
    delete tempProps.className;
    delete tempProps.hideWhenClickOut;

    return tempProps;
  }, [props]);

  const selectDisplayEl = useRef(null as null | HTMLLabelElement);
  const opEl = useRef(null as null | HTMLDivElement);

  const toggleShowOption = useCallback(
    (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
      event.stopPropagation();
      setShowOption(!showOption);
    },
    [showOption]
  );

  const [optionFixedPosition, setOptionFixedPosition] = useState({} as any);
  useEffect(() => {
    const changePosition = () => {
      const el = selectDisplayEl.current!;
      const chartClientRect = el.getBoundingClientRect();
      const diffTop = chartClientRect.top;
      const clientHeight = window.innerHeight || document.body.clientHeight;
      const elStyle = window.getComputedStyle(el);
      const elHeight = Number(elStyle.height.replace("px", ""));
      const elWidth = elStyle.width;
      const diffLeft = chartClientRect.left;

      let style;
      if (diffTop + elHeight / 2 > clientHeight - diffTop - elHeight / 2) {
        const optionEl = opEl.current!;
        const opelStyle = window.getComputedStyle(optionEl);
        const opHeight = Number(opelStyle.height.replace("px", ""));
        style = {
          left: `${diffLeft}px`,
          top: `${diffTop - opHeight - optionShift}px`,
          width: elWidth,
          transformOrigin: "bottom",
        };
      } else {
        style = {
          left: `${diffLeft}px`,
          top: `${diffTop + elHeight + optionShift}px`,
          width: elWidth,
          transformOrigin: "top",
        };
      }
      setOptionFixedPosition(style);
    };

    changePosition();
    window.addEventListener("scroll", changePosition, false);
    window.addEventListener("resize", changePosition, false);
    return () => {
      window.removeEventListener("scroll", changePosition);
      window.removeEventListener("resize", changePosition);
    };
  }, [optionShift, showOption]);

  const hideOption = useCallback(() => {
    setShowOption(false);
  }, []);

  useEffect(() => {
    if (!hideWhenClickOut) {
      return;
    }
    const el = document.documentElement;

    el.addEventListener("click", hideOption, false);
    return () => {
      el.removeEventListener("click", hideOption);
    };
  }, [hideOption, hideWhenClickOut]);

  const stopP = useCallback((e) => {
    e.stopPropagation();
  }, []);

  return (
    <label
      className={className}
      ref={selectDisplayEl}
      onClick={onClick || stopP}
    >
      <select
        {...realProps}
        ref={selectEl}
        onChange={onSelectChange}
        value={selectVal}
      >
        {options}
      </select>
      <div
        className="value_display"
        onClick={isDisabled ? undefined : toggleShowOption}
      >
        {valueDisplay}
      </div>
      <div className="options_box" ref={opEl} style={optionFixedPosition}>
        {tensorOptions}
      </div>
    </label>
  );
};

export default TensorSelect;
