import React from "react";
import { useMemo } from "react";
import "./button.scss";
import LoadingIco from "./loadingico";

type TensorButtonPropsSize = "small" | "middle" | "big";
type TensorButtonPropsBtnType =
  | "primary"
  | "danger"
  | "secondary"
  | "ghost"
  | "text"
  | "danger_border"
  | "danger_text";
interface TensorButtonProps
  extends React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  size?: TensorButtonPropsSize;
  btnType?: TensorButtonPropsBtnType;
  href?: string;
  ico?: string;
  loading?: boolean;
}

interface TensorButtonPropsA
  extends React.DetailedHTMLProps<
    React.AnchorHTMLAttributes<HTMLAnchorElement>,
    HTMLAnchorElement
  > {
  size?: TensorButtonPropsSize;
  btnType?: TensorButtonPropsBtnType;
  href?: string;
  ico?: string;
  loading?: boolean;
}

export type TensorButton_Props = TensorButtonProps | TensorButtonPropsA;

const TensorButtonClassName = "tensorButton";
export const TensorButton = (_props: TensorButton_Props) => {
  const { size = "middle", btnType = "primary", loading = false } = _props;
  const props = _props as any;

  const isDisabled = useMemo(() => {
    if (props.hasOwnProperty("disabled")) {
      if (props.disabled === true || undefined) {
        return true;
      }
      return false;
    }
    return false;
  }, [props]);

  const className = useMemo(() => {
    return `${TensorButtonClassName} ${
      props.href ? "anchor" : ""
    }  ${btnType} ${size}  ${loading ? "loading" : ""} ${
      isDisabled ? "disable" : ""
    }  ${props.className || ""}`;
  }, [props.className, size, btnType, isDisabled, props.href, loading]);

  const [realProps, realChildren] = useMemo(() => {
    const tempProps = {
      ...props,
      className,
    };

    if (props.loading) {
      tempProps.disabled = true;
    }

    delete tempProps.size;
    delete tempProps.btnType;
    delete tempProps.children;
    delete tempProps.ico;
    delete tempProps.loading;

    let childrenEl = props.ico ? (
      <>
        <img src={props.ico} alt="ico" className="ico" />
        {props.children}
      </>
    ) : (
      props.children
    );

    childrenEl = props.loading ? (
      <>
        <LoadingIco />
        {childrenEl}
      </>
    ) : (
      childrenEl
    );

    return [tempProps, childrenEl];
  }, [props, className]);

  if (props.href) {
    // eslint-disable-next-line jsx-a11y/anchor-has-content
    return <a {...realProps}>{realChildren}</a>;
  }
  return <button {...realProps}>{realChildren}</button>;
};

export default TensorButton;
