import React, { useCallback, useEffect, useMemo, useState } from "react";
import TensorButton, { TensorButton_Props } from "./buttons";
import "./modal.scss";

interface TensorModalProps {
  show?: boolean;
  className?: string;
  onShowStatusChange?: (showStatus: boolean) => void;
  closemodalfun?: () => void;
  topDistanceVh?: number;
  mask?: boolean;
  maskClose?: boolean;
  contentComp?: JSX.Element;
  distoryWhenHide?: boolean;
  width?: string;
}

const TensorModalClassName = "tensorModal_base";

export const TensorModalBase = (props: TensorModalProps) => {
  const {
    show,
    className: c_className,
    onShowStatusChange,
    closemodalfun,
    topDistanceVh = 10,
    mask = true,
    maskClose = true,
    contentComp,
    distoryWhenHide = false,
    width = "auto",
  } = props;

  useEffect(() => {
    onShowStatusChange && onShowStatusChange(!!show);
  }, [show, onShowStatusChange]);

  const className = useMemo(() => {
    return `${TensorModalClassName} ${mask ? "mask" : ""} ${
      show ? "show" : ""
    } ${c_className || ""}`;
  }, [c_className, show, mask]);

  if (distoryWhenHide && !show) {
    return <></>;
  }
  return (
    <div className={className}>
      <div
        className="mask"
        onClick={maskClose && closemodalfun ? closemodalfun : undefined}
      />
      <div
        className="innercontent"
        style={{ top: `${topDistanceVh}vh`, width }}
      >
        {contentComp && React.cloneElement(contentComp, { closemodalfun })}
      </div>
    </div>
  );
};

interface TensorModalHasActionCompProps extends TensorModalProps {
  actionComp?: JSX.Element;
}

export const TensorModalHasActionComp = (
  props: TensorModalHasActionCompProps
) => {
  const { actionComp, show: defaultShow } = props;
  const [show, setShow] = useState(defaultShow);
  const toggleModal = useCallback(
    (e: any) => {
      setShow(!show);
    },
    [show]
  );

  const hideModal = useCallback(() => {
    setShow(false);
  }, []);

  const realModalProps = useMemo(() => {
    const tempProps = { ...props };
    delete tempProps.actionComp;
    delete tempProps.show;
    return tempProps;
  }, [props]);

  return (
    <div>
      {actionComp && React.cloneElement(actionComp, { onClick: toggleModal })}
      <TensorModalBase
        {...realModalProps}
        show={show}
        closemodalfun={hideModal}
      />
    </div>
  );
};

export interface TensorModalHasActionButtonProps
  extends TensorModalHasActionCompProps {
  buttonProps?: TensorButton_Props;
}
export const TensorModalHasActionButton = (
  props: TensorModalHasActionButtonProps
) => {
  const { onShowStatusChange, buttonProps } = props;
  const [btnType, setbtnType] = useState(buttonProps?.btnType);
  const _onShowStatusChange = useCallback(
    (showStatus: boolean) => {
      setbtnType(showStatus ? "secondary" : "primary");
      onShowStatusChange && onShowStatusChange(showStatus);
    },
    [onShowStatusChange]
  );

  return (
    <TensorModalHasActionComp
      {...props}
      actionComp={<TensorButton {...props.buttonProps} btnType={btnType} />}
      onShowStatusChange={_onShowStatusChange}
    />
  );
};

interface TensorModalActionButtonGroupProps {
  cancleBtnProps?: TensorButton_Props;
  sureBtnProps?: TensorButton_Props;
  onCancle?: () => void;
  onSure?: (e: any, closeFun?: any) => void;
}
export const TensorModalActionButtonGroup = (
  props: TensorModalActionButtonGroupProps
) => {
  const { cancleBtnProps, sureBtnProps, onCancle, onSure } = props;
  return (
    <div className="tensor_modal_action_button_group">
      {cancleBtnProps?.children ? (
        <TensorButton
          {...cancleBtnProps}
          onClick={onCancle}
          btnType="secondary"
        />
      ) : (
        <></>
      )}
      {sureBtnProps?.children ? (
        <TensorButton {...sureBtnProps} onClick={onSure} btnType="primary" />
      ) : (
        <></>
      )}
    </div>
  );
};

interface TensorModalHaseActionButtonGroupProps
  extends TensorModalActionButtonGroupProps {
  closemodalfun?: () => void;
  modelComp?: JSX.Element;
  className?: string;
  showCloseBtn?: boolean;
}
export const TensorModalHaseActionButtonGroup = (
  props: TensorModalHaseActionButtonGroupProps
) => {
  const {
    closemodalfun,
    onCancle,
    onSure,
    modelComp,
    className: c_className,
    showCloseBtn = true,
  } = props;

  const _onCancle = useCallback(() => {
    onCancle && onCancle();
    closemodalfun && closemodalfun();
  }, [closemodalfun, onCancle]);

  const _onSure = useCallback(
    (e: any) => {
      onSure && onSure(e, closemodalfun);
    },
    [onSure, closemodalfun]
  );

  const className = useMemo(() => {
    return `tensor_modal_has_action_buttongroup ${c_className || ""}`;
  }, [c_className]);

  return (
    <div className={className}>
      {showCloseBtn && (
        <i className="close" onClick={_onCancle}>
          X
        </i>
      )}

      {modelComp}
      <TensorModalActionButtonGroup
        {...props}
        onCancle={_onCancle}
        onSure={_onSure}
      />
    </div>
  );
};

interface TensorModalNormalProps extends TensorModalHaseActionButtonGroupProps {
  title?: string | JSX.Element;
  content?: string | JSX.Element;
}
export const TensorModalNormal = (props: TensorModalNormalProps) => {
  const { title, content } = props;

  return (
    <TensorModalHaseActionButtonGroup
      {...props}
      className={`tensor_modal_noraml ${props.className || ""}`}
      modelComp={
        <>
          <div className="head">
            <span className="title">{title}</span>
          </div>
          <div className="content">{content}</div>
        </>
      }
    />
  );
};

export default TensorModalHasActionButton;
