import React, { useEffect, useMemo, useRef } from "react";
import ReactDOM from "react-dom";
import "./tensorPopover.scss";
import { documentOffset } from "./util";

interface TensorPopoverProps
  extends React.DetailedHTMLProps<
    React.HTMLAttributes<HTMLDivElement>,
    HTMLDivElement
  > {
  content?: JSX.Element;
  trigger?: "click" | "hover";
  visible?: boolean;
  onVisibleChange?: (visible: boolean) => void;
}

const TensorPopoverClassName = "tensorPopover";

export const TensorPopover = (props: TensorPopoverProps) => {
  const { content, children, trigger, visible, onVisibleChange } = props;
  const boxel = useRef(null as null | HTMLDivElement);

  useEffect(() => {
    const el = boxel.current!;
    if (!onVisibleChange) {
      return;
    }
    const clickFun = () => {
      onVisibleChange(!visible);
    };
    const enterFun = () => {
      onVisibleChange(true);
    };
    const leaveFun = () => {
      onVisibleChange(false);
    };
    if (trigger === "click") {
      el.addEventListener("click", clickFun, false);
    } else {
      el.addEventListener("mouseenter", enterFun, false);
      el.addEventListener("mouseleave", leaveFun, false);
    }

    return () => {
      if (trigger === "click") {
        el.removeEventListener("click", clickFun);
      } else {
        el.removeEventListener("mouseenter", enterFun);
        el.removeEventListener("mouseleave", leaveFun);
      }
    };
  }, [trigger, onVisibleChange, visible]);

  useEffect(() => {
    const body = document.body;
    const pbox = document.createElement("div");
    pbox.style.position = "absolute";
    pbox.style.width = "100%";
    pbox.style.height = "0";
    pbox.style.top = "0";
    pbox.style.left = "0";
    pbox.style.overflow = "visiable";
    pbox.style.visibility = "visiable";

    const boxEl = boxel.current!;
    const { left, top } = documentOffset(boxEl);

    const popoverEl = document.createElement("div");
    popoverEl.style.position = "absolute";
    popoverEl.style.left = `${left}px`;
    popoverEl.style.top = `${top}px`;
    popoverEl.style.zIndex = "100";
    popoverEl.className = `popoverEl ${visible ? "show" : "hide"}`;

    ReactDOM.render(content || <></>, popoverEl);

    pbox.appendChild(popoverEl);
    body.appendChild(pbox);
    return () => {
      body.removeChild(pbox);
    };
  }, [content, visible]);

  const className = useMemo(() => {
    return `${TensorPopoverClassName}  ${props.className || ""}`;
  }, [props.className]);

  const realProps = useMemo(() => {
    const tempProps = {
      ...props,
      className,
    };
    delete tempProps.children;
    delete tempProps.content;
    delete tempProps.trigger;
    delete tempProps.visible;
    delete tempProps.onVisibleChange;

    return tempProps;
  }, [props, className]);

  return (
    <div {...realProps} ref={boxel}>
      {children}
    </div>
  );
};

export default TensorPopover;
