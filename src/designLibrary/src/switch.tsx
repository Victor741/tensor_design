import React, { useEffect, useRef, useState } from "react";
import { useMemo } from "react";
import "./switch.scss";

interface TensorSwitchProps
  extends React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > {
  label?: any;
}

const TensorSwitchClassName = "tensorSwitch";

const TensorSwitchItem = (props: TensorSwitchProps) => {
  const isDisabled = useMemo(() => {
    if (props.hasOwnProperty("disabled")) {
      if (props.disabled === true || undefined) {
        return true;
      }
      return false;
    }
    return false;
  }, [props]);

  const isChecked = useMemo(() => {
    if (props.hasOwnProperty("checked")) {
      if (props.checked === true || undefined) {
        return true;
      }
      return false;
    }
    return false;
  }, [props]);

  const className = useMemo(() => {
    return `${TensorSwitchClassName} ${isChecked ? "checked" : ""} ${
      isDisabled ? "disable" : ""
    } ${props.className || ""}`;
  }, [props.className, isDisabled, isChecked]);

  const realProps = useMemo(() => {
    const tempProps = {
      ...props,
      className,
    };
    delete tempProps.type;
    delete tempProps.label;
    return tempProps;
  }, [props, className]);

  return (
    <label className={className}>
      <input {...realProps} type="radio" />
      <span className="label">{props.label}</span>
    </label>
  );
};

export type TensorSwitchGroupList = [
  {
    label?: string;
    value: string;
  },
  {
    label?: string;
    value: string;
  }
];
export interface TensorSwitchGroupProps {
  name: string;
  onChange?: (val: string) => any;
  defaultValue?: string;
  disabled?: boolean;
  tensorSwitchList?: TensorSwitchGroupList;
  className?: string;
  size?: "small" | "normal";
}

export enum TensorSwitchVal {
  open = "open",
  close = "close",
}

const defaultList: TensorSwitchGroupList = [
  {
    label: "√",
    value: TensorSwitchVal.open,
  },
  {
    label: "×",
    value: TensorSwitchVal.close,
  },
];
const TensorSwitchGroupClassName = "tensorSwitchGroup";
export const TensorSwitch = (props: TensorSwitchGroupProps) => {
  const {
    defaultValue,
    onChange,
    name,
    tensorSwitchList = defaultList,
    size = "normal",
  } = props;
  const [checked, setChecked] = useState(
    defaultValue || tensorSwitchList[0].value
  );

  const onChangeFun = useRef(null as any);
  useEffect(() => {
    onChangeFun.current = onChange;
  }, [onChange]);

  const isDisabled = useMemo(() => {
    if (props.hasOwnProperty("disabled")) {
      if (props.disabled === true || undefined) {
        return true;
      }
      return false;
    }
    return false;
  }, [props]);

  const tensorSwitchs = useMemo(() => {
    return tensorSwitchList.map((item) => {
      const { label, value } = item;
      return (
        <TensorSwitchItem
          key={value}
          label={label}
          onChange={(e) => {
            const val = e.target.value;
            setChecked(val);
            onChangeFun.current && onChangeFun.current(val);
          }}
          checked={checked === value}
          name={name}
          value={value}
          disabled={isDisabled}
        />
      );
    });
  }, [tensorSwitchList, name, checked, isDisabled]);

  const isClose = useMemo(() => {
    return checked === tensorSwitchList[1].value;
  }, [checked, tensorSwitchList]);

  const className = useMemo(() => {
    return `${TensorSwitchGroupClassName} ${size}  ${
      isClose ? "isClose" : "isOpen"
    }  ${isDisabled ? "disabled" : ""} ${props.className || ""}`;
  }, [props.className, isClose, isDisabled, size]);

  return <div className={className}>{tensorSwitchs}</div>;
};

export default TensorSwitch;
