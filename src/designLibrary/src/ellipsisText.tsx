import React, { useEffect, useMemo, useRef, useState } from 'react';

interface TensorEllipsisProps
  extends React.DetailedHTMLProps<
    React.HTMLAttributes<HTMLSpanElement>,
    HTMLSpanElement
  > {
  children?: string;
  maxRow?: number;
  ellipsisStr?: string;
  onEllipsis?: (
    ellipsis: boolean,
    ellipsisText: string,
    fullText: string
  ) => void;
  preHidden?: boolean;
}

const TensorEllipsisClassName = 'tensorEllipsis';

export const TensorEllipsis = (props: TensorEllipsisProps) => {
  const {
    maxRow = 1,
    children: inchildren = '',
    ellipsisStr = '...',
    onEllipsis,
    className,
    style,
    preHidden,
    ...extraProps
  } = props;
  const pEl = useRef(null as null | HTMLSpanElement);
  const holdEl = useRef(null as null | HTMLSpanElement);

  const uText = useMemo(() => {
    if (!inchildren || inchildren.length === 0) {
      return '';
    }
    let txt = inchildren;
    if (Array.isArray(inchildren)) {
      txt = inchildren.join('');
    }
    return !preHidden ? txt : txt.split('').reverse().join('');
  }, [preHidden, inchildren]);

  const inheritStyle = useMemo(() => {
    return {
      fontFamily: 'inherit',
      fontSize: 'inherit',
      fontStyle: 'inherit',
      fontWeight: 'inherit',
      padding: 0,
      margin: 0,
    } as React.CSSProperties;
  }, []);

  const boxStyle = useMemo(() => {
    return Object.assign(
      {
        width: '100%',
        height: '100%',
        position: 'relative',
        display: 'flex',
        alignItems: 'center',
      } as React.CSSProperties,
      inheritStyle,
      style
    );
  }, [inheritStyle, style]);

  const holerStyle = useMemo(() => {
    return Object.assign(
      {
        display: 'inline-block',
        width: 'auto',
        opacity: 0,
        zIndex: -100,
        whiteSpace: 'nowrap',
        position: 'absolute',
      } as React.CSSProperties,
      inheritStyle
    );
  }, [inheritStyle]);

  const [text, setText] = useState('');

  useEffect(() => {
    const children = uText;
    const textLen = children.length;
    if (!children || textLen === 0) {
      setText(children);
      onEllipsis && onEllipsis(false, children, children);
      return;
    }
    const ptEl = pEl.current!;
    const parentEl = ptEl.parentNode! as any;
    parentEl.style.width = '100%';
    const holderEl = holdEl.current!;

    const mathRows = () => {
      const h_width = holderEl.clientWidth; // 文字的总长度
      // const h_height = holderEl.clientHeight; // 一行的高度
      // holderEl.style.marginBottom = `${-h_height}px`;

      const p_width =
        parentEl.clientWidth -
        parentEl.style.paddingLeft -
        parentEl.style.paddingRight; // 盒子的最大宽度

      const beishu = h_width === p_width ? 1 : h_width / p_width;
      let yihangzishu = Math.floor(textLen / beishu);
      const ellipsisStrLen = ellipsisStr.length;

      if (beishu <= 1 || beishu === Infinity) {
        setText(children);
        onEllipsis &&
          onEllipsis(
            children.length >= yihangzishu - ellipsisStrLen,
            children,
            children
          );
        return;
      }
      const quzhenghangshu = Math.ceil(beishu);
      yihangzishu = yihangzishu > 1 ? yihangzishu - 1 : yihangzishu;
      const rowstr: string[] = [];
      for (let i = 0; i < quzhenghangshu; i++) {
        const aa = yihangzishu * i;
        const bb = yihangzishu * (1 + i);
        if (bb <= textLen) {
          rowstr.push(children.slice(aa, bb));
        } else if (aa < textLen) {
          rowstr.push(children.slice(aa));
        } else {
          break;
        }
      }
      const rowstrLen = rowstr.length;

      const usedStrArr = rowstr.slice(0, maxRow);
      const endStr = usedStrArr[usedStrArr.length - 1];

      if (
        endStr.length <= ellipsisStrLen ||
        (rowstrLen === maxRow && endStr.length <= yihangzishu - ellipsisStrLen)
      ) {
        setText(children);
        onEllipsis &&
          onEllipsis(
            endStr.length >= yihangzishu - ellipsisStrLen,
            children,
            children
          );
      } else {
        const strIndex = children.indexOf(endStr);
        const usedStr = children.slice(0, strIndex + endStr.length);
        const strHasEllipsis = `${usedStr.slice(
          0,
          usedStr.length - ellipsisStrLen
        )}${ellipsisStr}`;

        setText(strHasEllipsis);
        onEllipsis && onEllipsis(true, strHasEllipsis, children);
      }
    };
    mathRows();
    window.addEventListener('resize', mathRows, false);
    return () => {
      window.removeEventListener('resize', mathRows);
    };
  }, [uText, maxRow, ellipsisStr, onEllipsis]);

  const showSpanStyle = useMemo(() => {
    if (!preHidden && maxRow === 1) {
      return Object.assign({}, inheritStyle, {
        display: 'inline-block',
        width: '100%',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
      } as React.CSSProperties);
    }
    return inheritStyle;
  }, [inheritStyle, maxRow, preHidden]);

  return (
    <span
      ref={pEl}
      {...extraProps}
      style={boxStyle}
      className={TensorEllipsisClassName}
    >
      <span style={holerStyle} ref={holdEl}>
        {uText}
      </span>
      <span style={showSpanStyle} className={className}>
        {!preHidden
          ? maxRow === 1
            ? uText
            : text
          : text.split('').reverse().join('')}
      </span>
    </span>
  );
};

export default TensorEllipsis;
