import React, { useMemo } from "react";
import "./input.scss";

interface TensorInputProps
  extends React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > {
  danger?: boolean;
  inputSize?: "middle" | "small";
  label?: string;
}

const TensorInputClassName = "tensorInput";

export const TensorInput = (props: TensorInputProps) => {
  const { inputSize = "middle", label } = props;
  const isDisabled = useMemo(() => {
    if (props.hasOwnProperty("disabled")) {
      if (props.disabled === true || undefined) {
        return true;
      }
      return false;
    }
    return false;
  }, [props]);

  const className = useMemo(() => {
    return `${TensorInputClassName} ${inputSize} ${
      props.danger ? "danger" : ""
    } ${isDisabled ? "disable" : ""} ${props.className || ""}`;
  }, [props.className, isDisabled, props.danger, inputSize]);

  const realProps = useMemo(() => {
    const tempProps = {
      ...props,
    };
    delete tempProps.danger;
    delete tempProps.inputSize;
    delete tempProps.inputSize;
    delete tempProps.className;
    delete tempProps.label;

    return tempProps;
  }, [props]);
  return (
    <label className={className}>
      {label && <span className="input_label">{label}</span>}
      <input {...realProps} />
    </label>
  );
};

export default TensorInput;
