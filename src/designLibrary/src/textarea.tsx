import React, { useMemo } from "react";
import "./textarea.scss";

interface TensorTextareaProps
  extends React.DetailedHTMLProps<
    React.TextareaHTMLAttributes<HTMLTextAreaElement>,
    HTMLTextAreaElement
  > {
  danger?: boolean;
}

const TensorTextareaClassName = "tensorTextarea";

export const TensorTextarea = (props: TensorTextareaProps) => {
  const isDisabled = useMemo(() => {
    if (props.hasOwnProperty("disabled")) {
      if (props.disabled === true || undefined) {
        return true;
      }
      return false;
    }
    return false;
  }, [props]);

  const className = useMemo(() => {
    return `${TensorTextareaClassName} ${props.danger ? "danger" : ""} ${
      isDisabled ? "disable" : ""
    } ${props.className || ""}`;
  }, [props.className, isDisabled, props.danger]);

  const realProps = useMemo(() => {
    const tempProps = {
      ...props,
      className,
    };
    return tempProps;
  }, [props, className]);
  return <textarea {...realProps} />;
};

export default TensorTextarea;
