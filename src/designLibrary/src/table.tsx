import React, { useMemo } from "react";
import "./table.scss";

interface TensorTableProps {
  className?: string;
  data?: any[];
  columns: {
    name?: string;
    selector?: string;
    sortable?: boolean;
    onSort?: () => void;
    sortFunction?: (a: any, b: any) => number;
    cell?: (item: any) => any;
    style?: any;
    className?: string;
    headCell?: (headItem: any) => any;
    headStyle?: any;
    headClassName?: string;
  }[];
  header?: JSX.Element;
  pagination?: boolean;
}

const TensorTableClassName = "tensorTable";

export const TensorTable = (props: TensorTableProps) => {
  const { data = [], columns } = props;

  const tableHead = useMemo(() => {
    const headItems = columns.map((c_item, index) => {
      const {
        name,
        selector,
        sortable,
        onSort,
        sortFunction,
        headCell,
        headStyle,
        headClassName: h_className,
      } = c_item;
      return (
        <div
          className={`head_item  ${sortable ? "sortable" : ""} ${
            h_className || ""
          }`}
          style={headStyle}
          key={`${name}_${index}`}
        >
          {headCell ? headCell(c_item) : name || ""}
        </div>
      );
    });
    return <div className="head_items_box">{headItems}</div>;
  }, [columns]);

  const dataList = useMemo(() => {
    return data.map((d_item, index) => {
      const row = columns.map((c_item, c_index) => {
        const { selector, cell, style, className: col_className } = c_item;
        const itemData = selector ? d_item[selector] : undefined;
        return (
          <div
            className={`data_item ${col_className || ""}`}
            style={style}
            key={`${selector}_ ${c_index}`}
          >
            {cell ? cell(d_item) : itemData || ""}
          </div>
        );
      });

      return (
        <div className="data_row_items" key={index}>
          {row}
        </div>
      );
    });
  }, [columns, data]);

  const className = useMemo(() => {
    return `${TensorTableClassName} ${props.className || ""}`;
  }, [props.className]);

  return (
    <div className={className}>
      {tableHead}
      <div className="dataListBox">{dataList}</div>
    </div>
  );
};

export default TensorTable;
